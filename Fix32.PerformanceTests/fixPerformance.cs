﻿// <copyright file="fixPerformance.cs" company="BovineLabs">
// Copyright (c) BovineLabs. All rights reserved.
// </copyright>

namespace BovineLabs.Fix32.PerformanceTests
{
    using NUnit.Framework;
    using Unity.Burst;
    using Unity.Collections;
    using Unity.Jobs;
    using Unity.PerformanceTesting;
    using Random = UnityEngine.Random;

    public class fixPerformance
    {
        private const int Count = 10000000;
        private static readonly float AddMin = (float)(fix.MinValue / 2);
        private static readonly float AddMax = (float)(fix.MaxValue / 2);

        private static readonly float MultiMin = -100;
        private static readonly float MultiMax = 100;

        private static readonly float DivMin = 0.1f;
        private static readonly float DivMax = 1000;

        [Test]
        [Performance]
        public void AddFix()
        {
            NativeArray<fix> input = default;
            NativeArray<fix> output = default;

            Measure.Method(() =>
                {
                    new AddFixJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<fix>(Count, Allocator.TempJob);
                    output = new NativeArray<fix>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(AddMin, AddMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void AddFloat()
        {
            NativeArray<float> input = default;
            NativeArray<float> output = default;

            Measure.Method(() =>
                {
                    new AddFloatJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<float>(Count, Allocator.TempJob);
                    output = new NativeArray<float>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(AddMin, AddMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void SubFix()
        {
            NativeArray<fix> input = default;
            NativeArray<fix> output = default;

            Measure.Method(() =>
                {
                    new SubFixJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<fix>(Count, Allocator.TempJob);
                    output = new NativeArray<fix>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(AddMin, AddMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void SubFloat()
        {
            NativeArray<float> input = default;
            NativeArray<float> output = default;

            Measure.Method(() =>
                {
                    new SubFloatJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<float>(Count, Allocator.TempJob);
                    output = new NativeArray<float>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(AddMin, AddMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void MultiFix()
        {
            NativeArray<fix> input = default;
            NativeArray<fix> output = default;

            Measure.Method(() =>
                {
                    new MultiFixJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<fix>(Count, Allocator.TempJob);
                    output = new NativeArray<fix>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(MultiMin, MultiMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void MultiFloat()
        {
            NativeArray<float> input = default;
            NativeArray<float> output = default;

            Measure.Method(() =>
                {
                    new MultiFloatJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<float>(Count, Allocator.TempJob);
                    output = new NativeArray<float>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(MultiMin, MultiMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void DivFix()
        {
            NativeArray<fix> input = default;
            NativeArray<fix> output = default;

            Measure.Method(() =>
                {
                    new DivFixJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<fix>(Count, Allocator.TempJob);
                    output = new NativeArray<fix>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(DivMin, DivMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [Test]
        [Performance]
        public void DivFloat()
        {
            NativeArray<float> input = default;
            NativeArray<float> output = default;

            Measure.Method(() =>
                {
                    new DivFloatJob
                        {
                            Input = input,
                            Output = output,
                        }
                        .Schedule().Complete();
                })
                .SetUp(() =>
                {
                    input = new NativeArray<float>(Count, Allocator.TempJob);
                    output = new NativeArray<float>(Count, Allocator.TempJob);

                    for (var i = 0; i < Count; i++)
                    {
                        input[i] = Random.Range(DivMin, DivMax);
                    }
                })
                .CleanUp(() =>
                {
                    input.Dispose();
                    output.Dispose();
                })
                .Run();
        }

        [BurstCompile]
        private struct AddFloatJob : IJob
        {
            public NativeArray<float> Input;
            public NativeArray<float> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] + this.Input[i];
                }
            }
        }

        [BurstCompile]
        private struct AddFixJob : IJob
        {
            public NativeArray<fix> Input;
            public NativeArray<fix> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] + this.Input[i];
                }
            }
        }

        [BurstCompile]
        private struct SubFloatJob : IJob
        {
            public NativeArray<float> Input;
            public NativeArray<float> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] - 12435;
                }
            }
        }

        [BurstCompile]
        private struct SubFixJob : IJob
        {
            public NativeArray<fix> Input;
            public NativeArray<fix> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] - 12435;
                }
            }
        }

        [BurstCompile]
        private struct MultiFloatJob : IJob
        {
            public NativeArray<float> Input;
            public NativeArray<float> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] * this.Input[i];
                }
            }
        }

        [BurstCompile]
        private struct MultiFixJob : IJob
        {
            public NativeArray<fix> Input;
            public NativeArray<fix> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                        this.Output[i] = this.Input[i] * this.Input[i];
                }
            }
        }

        [BurstCompile]
        private struct DivFloatJob : IJob
        {
            public NativeArray<float> Input;
            public NativeArray<float> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                    this.Output[i] = this.Input[i] / (1 + this.Input[i]);
                }
            }
        }

        [BurstCompile]
        private struct DivFixJob : IJob
        {
            public NativeArray<fix> Input;
            public NativeArray<fix> Output;

            public void Execute()
            {
                for (var i = 0; i < Count; i++)
                {
                    this.Output[i] = this.Input[i] / (1 + this.Input[i]);
                }
            }
        }
    }
}
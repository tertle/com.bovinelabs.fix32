﻿#define FIXMATH_NO_OVERFLOW
#define FIXMATH_NO_ROUNDING

namespace BovineLabs.Fix32
{
    using System;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using Unity.Mathematics;

    /// <summary>
    /// Ported version of https://github.com/asik/FixedMath.Net
    /// To use 32 bits and variable decimal places.
    /// </summary>
    [System.Serializable]
    public struct fix : IEquatable<fix>, IComparable<fix>, IFormattable
    {
        /// <summary>
        /// The number of bits used by the decimal.
        /// </summary>
        public const int Shift = 12;
        private const int DecimalPlacesPlus1 = Shift + 1;
        private const int IntSignBit = 1 << (Shift - 1); // 0x8000; // 16th bit
        private const uint Invert15Mask = ~(uint)((1 << (Shift + 4)) - 1); // 0xFFF00000
#if !FIXMATH_NO_OVERFLOW
        private const int MultiSign = 32 + Shift - 1;
#endif

        private readonly int rawValue;

        /*public static readonly fix FourDivPi = new fix(0x145F3);
        public static readonly fix FourDivPi2 = new fix(0xFFFF9840);
        public static readonly fix X4CorrectionComponent = new fix(0x399A);
        public static readonly fix PiDiv4 = new fix(0x0000C90F);
        public static readonly fix ThreePiDiv4 = new fix(0x00025B2F);*/

        public static readonly fix MaxValue = new fix(int.MaxValue);
        public static readonly fix MinValue = new fix(int.MinValue);
        public static readonly fix Overflow = new fix(int.MinValue);

        //public static readonly fix Pi = new fix(205887);
        //public static readonly fix E = new fix(178145);

        //public static readonly fix PI = math.PI;
        //public static readonly fix E = math.E;
        public static readonly fix One = new fix(1 << Shift);
        public static readonly fix Zero = new fix(0);

        private fix(int rawValue)
        {
            this.rawValue = rawValue;
        }

        private fix(uint rawValue)
        {
            this.rawValue = (int)rawValue;
        }

        internal int RawValue => this.rawValue;

        public static implicit operator fix(int a)
        {
            return new fix(a * One.rawValue);
        }

        public static explicit operator float(fix a)
        {
            return (float)a.rawValue / One.rawValue;
        }

        public static explicit operator double(fix a)
        {
            return (double)a.rawValue / One.rawValue;
        }

        public static explicit operator decimal(fix a)
        {
            return (decimal)a.rawValue / One.rawValue;
        }

        public static explicit operator int(fix a)
        {
#if !FIXMATH_NO_ROUNDING
            return a.rawValue >> Shift;
#else
            if (a.rawValue >= 0) {
                return (a.rawValue + (One.rawValue >> 1)) / One.rawValue;
            }
            return (a.rawValue - (One.rawValue >> 1)) / One.rawValue;
#endif
        }

        public static implicit operator fix(float a)
        {
            var temp = a * One.rawValue;
#if !FIXMATH_NO_ROUNDING
            temp += (temp >= 0) ? 0.5f : -0.5f;
#endif
            return new fix((int)temp);
        }

        public static implicit operator fix(double a)
        {
            var temp = a * One.rawValue;
#if !FIXMATH_NO_ROUNDING
            temp += (temp >= 0) ? 0.5f : -0.5f;
#endif
            return new fix((int)temp);
        }

        public static fix operator /(fix x, fix y)
        {
            if (y.rawValue == MinValue || y.rawValue == 0)
            {
                return Zero;
            }

            int res = (int)(((long)x.rawValue << Shift) / y.rawValue);
            return new fix(res);

            /*if (y.rawValue == 0)
            {
                return MinValue;
            }

            var product = (long)x.rawValue << Shift; // convert into Q32 number
            product /= y.rawValue; // is now a Q16 number again


#if !FIXMATH_NO_OVERFLOW
            // The upper X bits should all be the same (the sign).
            var upper = (uint)(product >> MultiSign);
#endif

#if !FIXMATH_NO_OVERFLOW
            if (product < 0)
            {
#if !FIXMATH_NO_OVERFLOW
                if (~upper != 0)
                {
                    return Overflow;
                }
#endif

            }
            else
            {
#if !FIXMATH_NO_OVERFLOW
                if (upper != 0)
                {
                    return Overflow;
                }
#endif
            }
#endif
            return new fix((int)product);*/

            // This uses a hardware 32/32 bit division multiple times, until we have
            // computed all the bits in (a<<17)/b. Usually this takes 1-3 iterations.
            var a = x.rawValue;
            var b = y.rawValue;

            if (b == 0)
            {
                return MinValue;
            }

            var remainder = (uint)((a >= 0) ? a : (-a));
            var divider = (uint)((b >= 0) ? b : (-b));
            var quotient = 0U;
            var bitPos = Shift + 1;

            // Kick-start the division a bit.
            // This improves speed in the worst-case scenarios where N and D are large
            // It gets a lower estimate for the result by N/(D >> 17 + 1).
            if ((divider & Invert15Mask) != 0)
            {
                var shiftedDiv = (divider >> DecimalPlacesPlus1) + 1;
                quotient = remainder / shiftedDiv;
                remainder -= (uint)(((ulong)quotient * divider) >> DecimalPlacesPlus1);
            }

            // If the divider is divisible by 2^n, take advantage of it.
            while ((divider & 0xF) == 0 && bitPos >= 4)
            {
                divider >>= 4;
                bitPos -= 4;
            }

            while (remainder != 0 && bitPos >= 0)
            {
                // Shift remainder as much as we can without overflowing
                int shift = Clz(remainder);
                if (shift > bitPos)
                {
                    shift = bitPos;
                }

                remainder <<= shift;
                bitPos -= shift;

                var div = remainder / divider;
                remainder = remainder % divider;
                quotient += div << bitPos;

#if !FIXMATH_NO_OVERFLOW
                if ((div & ~(0xFFFFFFFF >> bitPos)) != 0)
                {
                    return Overflow;
                }
#endif

                remainder <<= 1;
                bitPos--;
            }

#if !FIXMATH_NO_ROUNDING
            // Quotient is always positive so rounding is easy
            quotient++;
#endif

            var result = (int)(quotient >> 1);

            // Figure out the sign of the result
            if (((a ^ b) & 0x80000000) != 0)
            {
#if !FIXMATH_NO_OVERFLOW
                if (result == MinValue.rawValue)
                {
                    return Overflow;
                }
#endif

                result = -result;
            }

            return new fix(result);
        }

        // Since this is .NET, we can assume 64-bit arithmetic is supported
        public static fix operator *(fix x, fix y)
        {
            var product = (long)x.rawValue * y.rawValue;

#if !FIXMATH_NO_OVERFLOW
            // The upper X bits should all be the same (the sign).
            var upper = (uint)(product >> MultiSign);
#endif

#if !FIXMATH_NO_OVERFLOW || !FIXMATH_NO_ROUNDING
            if (product < 0)
            {
#if !FIXMATH_NO_OVERFLOW
                if (~upper != 0)
                {
                    return Overflow;
                }
#endif

                // This adjustment is required in order to round -1/2 correctly
#if !FIXMATH_NO_ROUNDING
                product--;
#endif
            }
            else
            {
#if !FIXMATH_NO_OVERFLOW
                if (upper != 0)
                {
                    return Overflow;
                }
#endif
            }
#endif

#if FIXMATH_NO_ROUNDING
            return new fix((int)(product >> Shift));
#else
            var result = product >> Shift;
            result += (product & IntSignBit) >> (Shift - 1);

            return new fix((int)result);
#endif
        }

        public static fix operator +(fix x, fix y)
        {
#if FIXMATH_NO_OVERFLOW
            return new fix(x.rawValue + y.rawValue);
#else
            var sum = x.rawValue + y.rawValue;

            // Overflow can only happen if sign of a == sign of b, and then
            // it causes sign of sum != sign of a.
            if ((((x.rawValue ^ y.rawValue) & int.MinValue) == 0) && (((x.rawValue ^ sum) & 0x80000000) != 0))
            {
                //throw new OverflowException();
                return Overflow;
            }

            return new fix(sum);
#endif
        }

        public static fix operator -(fix x, fix y)
        {
#if FIXMATH_NO_OVERFLOW
            return new fix(x.rawValue - y.rawValue);
#else
            var diff = x.rawValue - y.rawValue;

            // Overflow can only happen if sign of a != sign of b, and then
            // it causes sign of sum != sign of a.
            if ((((x.rawValue ^ y.rawValue) & int.MinValue) != 0) && (((x.rawValue ^ diff) & 0x80000000) != 0))
            {
                throw new OverflowException();
                return Overflow;
            }

            return new fix(diff);
#endif
        }

        public static fix operator %(fix x, fix y)
        {
            return new fix(x.rawValue % y.rawValue);
        }

        public static fix operator >>(fix x, int shift)
        {
            return new fix(x.rawValue >> shift);
        }

        public static fix operator <<(fix x, int shift)
        {
            return new fix(x.rawValue << shift);
        }

        public static fix operator -(fix x)
        {
            return new fix(-x.rawValue);
        }

        public static bool operator >(fix x, fix y)
        {
            return x.rawValue > y.rawValue;
        }

        public static bool operator <(fix x, fix y)
        {
            return x.rawValue < y.rawValue;
        }

        public static bool operator >=(fix x, fix y)
        {
            return x.rawValue >= y.rawValue;
        }

        public static bool operator <=(fix x, fix y)
        {
            return x.rawValue <= y.rawValue;
        }

        public static bool operator ==(fix x, fix y)
        {
            return x.rawValue == y.rawValue;
        }

        public static bool operator !=(fix x, fix y)
        {
            return x.rawValue != y.rawValue;
        }

        public static fix operator ++(fix x)
        {
            return x + One;
        }

        public static fix operator --(fix x)
        {
            return x - One;
        }

        public static fix Abs(fix x)
        {
            // branchless implementation, see http://www.strchr.com/optimized_abs_function
            int mask = x.rawValue >> 31;
            return new fix((x.rawValue + mask) ^ mask);
        }

        public static fix Floor(fix x)
        {
            return new fix((int)((ulong)x.rawValue & 0xFFFF0000UL));
        }

        public static fix Ceil(fix x)
        {
            return new fix((int)(((ulong)x.rawValue & 0xFFFF0000UL) +
                 (((ulong)x.rawValue & 0x0000FFFFUL) != 0UL ? (ulong)One.rawValue : 0UL)));
        }

        public static fix Min(fix x, fix y)
        {
            return x.rawValue < y.rawValue ? x : y;
        }

        public static fix Max(fix x, fix y)
        {
            return x.rawValue > y.rawValue ? x : y;
        }

        public static fix Clamp(fix x, fix min, fix max)
        {
            return Min(Max(x, min), max);
        }

        public static fix SAdd(fix a, fix b)
        {
            var result = a + b;

            if (result == Overflow)
            {
                return (a > Zero) ? MaxValue : MinValue;
            }

            return result;
        }

        public static fix SSub(fix a, fix b)
        {
            var result = a - b;

            if (result == Overflow)
            {
                return (a > Zero) ? MaxValue : MinValue;
            }

            return result;
        }

        public static fix SMul(fix a, fix b)
        {
            var result = a * b;

            if (result == Overflow)
            {
                return (a >= Zero) == (b >= Zero) ? MaxValue : MinValue;
            }

            return result;
        }

        private static byte Clz(uint x)
        {
            byte result = 0;
            if (x == 0)
            {
                return 32;
            }

            while ((x & 0xF0000000) == 0)
            {
                result += 4;
                x <<= 4;
            }

            while ((x & 0x80000000) == 0)
            {
                result += 1;
                x <<= 1;
            }

            return result;
        }

        public static fix SDiv(fix inArg0, fix inArg1)
        {
            var result = inArg0 / inArg1;

            if (result == Overflow)
            {
                return (inArg0 >= Zero) == (inArg1 >= Zero) ? MaxValue : MinValue;
            }

            return result;
        }

        public static fix FromRaw(int i)
        {
            return new fix(i);
        }

        public override string ToString()
        {
            // Using Decimal.ToString() instead of float or double because decimal is
            // also implemented in software. This guarantees a consistent string representation.
            return ((decimal)this).ToString(CultureInfo.InvariantCulture);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return $"fix({((decimal)this).ToString(format, formatProvider)})";
        }

        public bool Equals(fix other)
        {
            return this.rawValue == other.rawValue;
        }

        public override bool Equals(object obj)
        {
            return obj is fix other && this.Equals(other);
        }

        public int CompareTo(fix other)
        {
            return this.rawValue.CompareTo(other.rawValue);
        }

        public override int GetHashCode()
        {
            return this.rawValue;
        }
    }
}
﻿#define FIXMATH_NO_OVERFLOW
#define FIXMATH_NO_ROUNDING

namespace BovineLabs.Packages.Fix32.Tests
{
    using System;
    using BovineLabs.Fix32;
    using NUnit.Framework;
    using UnityEngine;

    public class fixTests
    {
        private readonly int[] testCases = {
            // Small numbers
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            -1, -2, -3, -4, -5, -6, -7, -8, -9, -10,

            // Integer numbers
            0x10000, -0x10000, 0x20000, -0x20000, 0x30000, -0x30000,
            0x40000, -0x40000, 0x50000, -0x50000, 0x60000, -0x60000,

            // Fractions (1/2, 1/4, 1/8)
            0x8000, -0x8000, 0x4000, -0x4000, 0x2000, -0x2000,

            // Problematic carry
            0xFFFF, -0xFFFF, 0x1FFFF, -0x1FFFF, 0x3FFFF, -0x3FFFF,

            // Smallest and largest values
            int.MaxValue, int.MinValue,

            // Large random numbers
            831858892, 574794913, 2147272293, -469161054, -961611615,
            1841960234, 1992698389, 520485404, 560523116, -2094993050,
            -876897543, -67813629, 2146227091, 509861939, -1073573657,

            // Small random numbers
            -14985, 30520, -83587, 41129, 42137, 58537, -2259, 84142,
            -28283, 90914, 19865, 33191, 81844, -66273, -63215, -44459,
            -11326, 84295, 47515, -39324,

            // Tiny random numbers
            -171, -359, 491, 844, 158, -413, -422, -737, -575, -330,
            -376, 435, -311, 116, 715, -1024, -487, 59, 724, 993
        };

        static fix Delta(fix a, fix b)
        {
            return a > b ? a - b : b - a;
        }

#if !FIXMATH_NO_ROUNDING
        static readonly fix MaxDelta = fix.FromRaw(1);
#else
        static readonly fix MaxDelta = fix.FromRaw(0);
#endif

        private const int IntSignBit = 1 << (fix.Shift - 1);

        [Test]
        public void BasicMultiplicationPosPos()
        {
            MultiplicationTest(5, 5, 25);
        }

        [Test]
        public void BasicMultiplicationNegPos()
        {
            MultiplicationTest(-5, 5, -25);
        }

        [Test]
        public void BasicMultiplicationNegNeg()
        {
            MultiplicationTest(-5, -5, 25);
        }

        [Test]
        public void BasicMultiplicationPosNeg()
        {
            MultiplicationTest(5, -5, -25);
        }

        static void MultiplicationTest(int v1, int v2, int expected)
        {
            var a1 = (fix)v1;
            var a2 = (fix)v2;
            var expectedF = (fix)expected;
            var actual = a1 * a2;
            Assert.AreEqual(expectedF, actual);
        }

        static void MultiplicationTestRaw(int v1, int v2, int expected)
        {
            var a1 = fix.FromRaw(v1);
            var a2 = fix.FromRaw(v2);
            var expectedF = fix.FromRaw(expected);
            var actual = a1 * a2;
            Assert.AreEqual(expectedF, actual);
        }

#if !FIXMATH_NO_ROUNDING
        [Test]
        public void MultiplicationRoundingCornerCases()
        {
            MultiplicationTestRaw(2, IntSignBit, 1);
            MultiplicationTestRaw(-2, IntSignBit, -1);
            MultiplicationTestRaw(3, IntSignBit, 2);
            MultiplicationTestRaw(-3, IntSignBit, -2);
            MultiplicationTestRaw(2, IntSignBit - 1, 1);
            MultiplicationTestRaw(-2, IntSignBit - 1, -1);
            MultiplicationTestRaw(2, IntSignBit + 1, 1);
            MultiplicationTestRaw(-2, IntSignBit + 1, -1);
        }
#endif

        [Test]
        public void MultiplicationTestCases()
        {
            this.RunAllTestCases((f1, f2) => f1 * f2, (d1, d2) => d1 * d2, "*");
        }

        void RunAllTestCases(Func<fix, fix, fix> fix16Op, Func<double, double, double> doubleOp, string op)
        {
            for (var i = 0; i < testCases.Length; i++) {
                for (var j = 0; j < testCases.Length; j++) {
                    var a = fix.FromRaw(testCases[i]);
                    var b = fix.FromRaw(testCases[j]);
                    var result = fix16Op(a, b);

                    var fa = (double)a;
                    var fb = (double)b;

                    var fresult = (fix)doubleOp(fa, fb);

                    var max = (double)(fix.MaxValue);
                    var min = (double)(fix.MinValue);

                    if (Delta(fresult, result) > MaxDelta) {
                        if (doubleOp(fa, fb) > max || doubleOp(fa, fb) < min) {
#if !FIXMATH_NO_OVERFLOW
                            if (fix.Overflow != result)
                            {
                                result = fix16Op(a, b);
                            }
                            Assert.AreEqual(fix.Overflow, result);
#endif
                            // Legitimate overflow
                            continue;
                        }

                        Assert.True(false, string.Format("{0} {1} {2} = {3}\n{4} {1} {5} = {6}", a, op, b, result, fa, fb, fresult));
                    }
                }
            }
        }


        [Test]
        public void BasicDivisionPosPos()
        {
            DivisionTest(15, 5, 3);
        }

        [Test]
        public void BasicDivisionNegPos()
        {
            DivisionTest(-15, 5, -3);
        }

        [Test]
        public void BasicDivisionPosNeg()
        {
            DivisionTest(15, -5, -3);
        }

        [Test]
        public void BasicDivisionNegNeg()
        {
            DivisionTest(-15, -5, 3);
        }

        static void DivisionTest(int v1, int v2, int expected)
        {
            var a1 = (fix)v1;
            var a2 = (fix)v2;
            var expectedF = (fix)expected;
            var actual = a1 / a2;
            Assert.AreEqual(expectedF, actual);
        }

        static void DivisionTestRaw(int v1, int v2, int expected)
        {
            var a1 = fix.FromRaw(v1);
            var a2 = fix.FromRaw(v2);
            var expectedF = fix.FromRaw(expected);
            var actual = a1 / a2;
            Assert.AreEqual(expectedF, actual);
        }

        static void DivisionTestRaw(int v1, fix v2, int expected)
        {
            var a1 = fix.FromRaw(v1);
            var expectedF = fix.FromRaw(expected);
            var actual = a1 / v2;
            Assert.AreEqual(expectedF, actual);
        }

#if !FIXMATH_NO_ROUNDING
        [Test]
        public void DivisionRoundingCornerCases()
        {
            DivisionTestRaw(0, 10, 0);
            DivisionTestRaw(1, (fix)(2), 1);
            DivisionTestRaw(-1, (fix)(2), -1);
            DivisionTestRaw(1, (fix)(-2), -1);
            DivisionTestRaw(-1, (fix)(-2), 1);
            DivisionTestRaw(3, (fix)(2), 2);
            DivisionTestRaw(-3, (fix)(2), -2);
            DivisionTestRaw(3, (fix)(-2), -2);
            DivisionTestRaw(-3, (fix)(-2), 2);
            DivisionTestRaw(2, IntSignBit - 1, 4);
            DivisionTestRaw(-2, IntSignBit - 1, -4);
            DivisionTestRaw(2, IntSignBit + 1, 4);
            DivisionTestRaw(-2, IntSignBit + 1, -4);
        }
#endif

        [Test]
        public void DivisionTestCases()
        {
            this.RunAllTestCases((f1, f2) => f1 / f2, (d1, d2) => d1 / d2, "/");
        }

        [Test]
        public void AdditionTestCases()
        {
            this.RunAllTestCases((f1, f2) => f1 + f2, (d1, d2) => d1 + d2, "+");
        }

        [Test]
        public void SubstractionTestCases()
        {
            this.RunAllTestCases((f1, f2) => f1 - f2, (d1, d2) => d1 - d2, "-");
        }

        [Test]
        public void SubstractionSign()
        {
            for (int i = -1; i <= 1; ++i)
            {
                for (int j = -1; j <= 1; ++j)
                {
                    var a1 = (fix)i;
                    var a2 = (fix)j;
                    var actual = (int)(a1 - a2);
                    var expected = i - j;
                    Assert.AreEqual(expected, actual);
                }
            }
        }

        [Test]
        public void AdditionSign()
        {
            for (int i = -1; i <= 1; ++i)
            {
                for (int j = -1; j <= 1; ++j)
                {
                    var a1 = (fix)i;
                    var a2 = (fix)j;
                    var actual = (int)(a1 + a2);
                    var expected = i + j;
                    Assert.AreEqual(expected, actual);
                }
            }
        }
    }
}
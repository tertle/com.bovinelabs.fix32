namespace BovineLabs.Fix32
{
    using System.Runtime.CompilerServices;

    /// <summary>
    /// The mathFix.
    /// </summary>
    public static class mathFix
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix dot(fix2 x, fix2 y)
        {
            return (x.x * y.x) + (x.y * y.y);
        }

        /// <summary>Returns the minimum of two fix values.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix min(fix x, fix y)
        {
            return x < y ? x : y;
        }

        /// <summary>Returns the component wise minimum of two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 min(fix2 x, fix2 y)
        {
            return new fix2(min(x.x, y.x), min(x.y, y.y));
        }

        /// <summary>Returns the maximum of two fix values.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix max(fix x, fix y)
        {
            return x > y ? x : y;
        }

        /// <summary>Returns the component wise maximum of two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 max(fix2 x, fix2 y)
        {
            return new fix2(max(x.x, y.x), max(x.y, y.y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        /// <summary>Returns the squared length of a float2 vector.</summary>
        public static fix lengthsq(fix2 x)
        {
            return dot(x, x);
        }
    }
}
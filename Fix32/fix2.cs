namespace BovineLabs.Fix32
{
    using System;
    using System.Runtime.CompilerServices;
    using Unity.Mathematics;

    [Serializable]
    public struct fix2 : IEquatable<fix2>, IComparable<fix2>, IFormattable
    {
        public fix x;
        public fix y;

        public fix2(fix x, fix y)
        {
            this.x = x;
            this.y = y;
        }

        public static implicit operator fix2(float2 f)
        {
            return new fix2(f.x, f.y);
        }

        public static implicit operator fix2(int2 f)
        {
            return new fix2(f.x, f.y);
        }

        public static explicit operator float2(fix2 f)
        {
            return new float2((float)f.x, (float)f.y);
        }

        public static fix2 operator *(fix2 lhs, fix2 rhs)
        {
            return new fix2(lhs.x * rhs.x, lhs.y * rhs.y);
        }

        /// <summary>Returns the result of a component wise addition operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator +(fix2 lhs, fix2 rhs)
        {
            return new fix2(lhs.x + rhs.x, lhs.y + rhs.y);
        }

        /// <summary>Returns the result of a component wise subtraction operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator -(fix2 lhs, fix2 rhs)
        {
            return new fix2(lhs.x - rhs.x, lhs.y - rhs.y);
        }

        /// <summary>Returns the result of a component wise division operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator /(fix2 lhs, fix2 rhs)
        {
            return new fix2(lhs.x / rhs.x, lhs.y / rhs.y);
        }

        /// <summary>Returns the result of a component wise modulus operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator %(fix2 lhs, fix2 rhs)
        {
            return new fix2(lhs.x % rhs.x, lhs.y % rhs.y);
        }

        /// <summary>Returns the result of a component wise increment operation on a fix2 vector.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator ++(fix2 val)
        {
            return new fix2(val.x + fix.One, val.y + fix.One);
        }

        /// <summary>Returns the result of a component wise decrement operation on a fix2 vector.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator --(fix2 val)
        {
            return new fix2(val.x - fix.One, val.y - fix.One);
        }

        /// <summary>Returns the result of a component wise less than operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator <(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x < rhs.x, lhs.y < rhs.y);
        }

        /// <summary>Returns the result of a component wise less or equal operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator <=(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x <= rhs.x, lhs.y <= rhs.y);
        }

        /// <summary>Returns the result of a component wise greater than operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator >(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x > rhs.x, lhs.y > rhs.y);
        }

        /// <summary>Returns the result of a component wise greater or equal operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator >=(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x >= rhs.x, lhs.y >= rhs.y);
        }

        /// <summary>Returns the result of a component wise unary minus operation on a fix2 vector.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator -(fix2 val)
        {
            return new fix2(-val.x, -val.y);
        }

        /// <summary>Returns the result of a component wise unary plus operation on a fix2 vector.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static fix2 operator +(fix2 val)
        {
            return new fix2(val.x, val.y);
        }

        /// <summary>Returns the result of a component wise equality operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator ==(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x == rhs.x, lhs.y == rhs.y);
        }

        /// <summary>Returns the result of a component wise not equal operation on two fix2 vectors.</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool2 operator !=(fix2 lhs, fix2 rhs)
        {
            return new bool2(lhs.x != rhs.x, lhs.y != rhs.y);
        }

        public override bool Equals(object obj)
        {
            return obj is fix2 other && this.Equals(other);
        }

        /// <inheritdoc />
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Equals(fix2 rhs)
        {
            return this.x == rhs.x && this.y == rhs.y;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return (int)math.hash(new int2(this.x.RawValue, this.y.RawValue));
        }

        public override string ToString()
        {
            return $"fix2({this.x.ToString()}, {this.y.ToString()})";
        }

        /// <inheritdoc />
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string ToString(string format, IFormatProvider formatProvider)
        {
            return $"fix2({this.x.ToString(format, formatProvider)}, {this.y.ToString(format, formatProvider)})";
        }

        public int CompareTo(fix2 other)
        {
            var xComparison = this.x.CompareTo(other.x);

            if (xComparison != 0)
            {
                return xComparison;
            }

            return this.y.CompareTo(other.y);
        }
    }
}